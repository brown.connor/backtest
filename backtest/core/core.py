import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import logging

logger = logging.getLogger(__name__)


class Backtest:
    """
    Library designed for the application of backtesting methods to systematic trading strategies.
    """
    def __init__(
            self,
            ord_df: pd.DataFrame,
            fee: float,
            sharpe: float = None
    ):
        """
        Initialise object. The order dataframe is of the form
        ------------------------
        | date | value | order |
        ------------------------
        |      |       |       |
        ------------------------
        |      |       |       |
        ------------------------
        where value is purposfully ambiguous as its interpretation will change form strategy to strategy (e.g. this
        could a "price", "spread", "rate").
        :param ord_df: pd.DataFrame, orders dataframe as described above
        :param fee: float, average fee paid on each trade
        :param sharpe: float, sharpe ratio
        """
        self.ord_df = ord_df
        self.fee = fee
        self.sharpe = sharpe

    def calc_position(self):
        """
        Calculate position at each point in time for order dataframe
        """
        self.ord_df['position'] = self.ord_df.order.cumsum()

    def calc_pnl(self):
        """
        Calculate pnl as though you were to exit the current position (i.e. you are position neutral in you observation
        of today's pnl)
        """
        long_or_short = np.sign(self.ord_df.order)
        self.ord_df['order_value'] = self.ord_df.value + long_or_short * self.fee
        self.ord_df['pnl'] = (-self.ord_df.order * self.ord_df.order_value).cumsum()
        self.ord_df.pnl += (self.ord_df.value - self.fee * np.sign(self.ord_df.position)) * self.ord_df.position

    def calc_sharpe(self):
        """
        Calculate the sharpe ratio of your strategy.
        """
        self.sharpe = self.ord_df.pnl.mean() / self.ord_df.pnl.std()
