FROM python:3.7

ADD . /app

WORKDIR /app

RUN pip install -U pip \
 && pip install pytest pytest.cov . \
 && rm -rf backtest.egg-info