from setuptools import setup, find_packages

setup(name='backtest',
      version='0.0.1',
      packages=find_packages(),
      install_requires=[
          'pandas>=0.25.0, < 0.26.0',
          'numpy >= 1.16.4, < 1.17.0',
          'matplotlib >= 3.1.0, < 3.2.0'
      ]
      )
