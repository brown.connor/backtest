import unittest
from backtest.core import Backtest
import pandas as pd


class TestCore(unittest.TestCase):
    @staticmethod
    def create_test_df():
        test_df = pd.DataFrame(data={'order': [0, 1, 1, 0, -2, 1, -1],
                                     'value': [10, 11, 13, 12.5, 11.5, 14, 13]
                                     },
                               index=pd.date_range('2019-07-01', '2019-07-9', freq='B'),
                               )
        test_df.index.name = 'date'
        return test_df

    @staticmethod
    def test_calc_pnl():
        test_df = TestCore.create_test_df()

        btest = Backtest(test_df, 0.25)
        btest.calc_position()
        btest.calc_pnl()

        exp = pd.Series([0., -0.5, 1., 0., -2., -2.5, -3.5],
                        index=test_df.index,
                        name='pnl'
                        )

        pd.testing.assert_series_equal(exp, btest.ord_df.pnl)

    def test_calc_sharpe(self):
        test_df = TestCore.create_test_df()

        btest = Backtest(test_df, 0.25)
        btest.calc_position()
        btest.calc_pnl()
        btest.calc_sharpe()

        self.assertAlmostEqual(-0.6620511, btest.sharpe)


if __name__ == '__main__':
    unittest.main()
